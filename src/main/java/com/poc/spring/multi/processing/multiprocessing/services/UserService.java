package com.poc.spring.multi.processing.multiprocessing.services;

import com.poc.spring.multi.processing.multiprocessing.entities.User;
import com.poc.spring.multi.processing.multiprocessing.repos.UserRepo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;

@Service
public class UserService {
    @Autowired
    private UserRepo userRepo;

    @Qualifier("taskExecutor")
    @Autowired
    private Executor executor;

    Logger logger = LoggerFactory.getLogger(UserService.class);

    @Async
    public CompletableFuture<List<User>> findAllUsers(){
        logger.info("Get List of user by {}", Thread.currentThread().getName());
        List<User> users = userRepo.findAll();
        return CompletableFuture.completedFuture(users);
    }

    public void saveUsersCF(MultipartFile file) throws Exception {
        long startTime = System.currentTimeMillis();
        for(int i =0; i< 2 ;i++){
            CompletableFuture
                    //.supplyAsync(() -> parseFile(file)) //// call this for java 8 default executor i.e. ForkJoinPool
                    .supplyAsync(() -> parseFile(file),executor) // call this for custom executor bean
                    .thenApply(data -> {
                        System.out.println(System.currentTimeMillis()+": main : thenApply Start , by " + Thread.currentThread());
                        List<User> users = userRepo.saveAll(data);
                        System.out.println(System.currentTimeMillis()+": main : thenApply Done , by " + Thread.currentThread());
                        return users;
                    }).thenAccept(data->{
                        System.out.println(System.currentTimeMillis()+": main : thenAccept Start , by " + Thread.currentThread());
                        data.forEach(System.out :: println);
                        System.out.println(System.currentTimeMillis()+": main : thenAccept Done , by " + Thread.currentThread());
                    });
        }
        long endTime = System.currentTimeMillis();
        logger.info(System.currentTimeMillis()+"Total time {}", (endTime-startTime));
    }

    private List<User> parseFile(MultipartFile file) {
        System.out.println(System.currentTimeMillis()+": parseFile : started , by "+Thread.currentThread());
        final List<User> users = new ArrayList<>();
        try{
            try(final BufferedReader br = new BufferedReader(new InputStreamReader(file.getInputStream()))){
                String line;
                while((line = br.readLine()) != null){
                    final String[] data  = line.split(",");
                    final User user = new User();
                    user.setName(data[0]);
                    user.setEmail(data[1]);
                    user.setGender(data[2]);
                    users.add(user);
                }
            }
        }catch (IOException ioe){
            logger.error("failed to parse CSV file {}", ioe);
            // throw new Exception("failed to parse CSV file {}", ioe);
        }
        System.out.println(System.currentTimeMillis()+": parseFile : completed , by "+Thread.currentThread());
        return users;
    }
}
