package com.poc.spring.multi.processing.multiprocessing;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
public class MultiProcessingApplication {
    public static void main(String[] args) {
        SpringApplication.run(MultiProcessingApplication.class, args);
    }
}
