package com.poc.spring.multi.processing.multiprocessing.repos;

import com.poc.spring.multi.processing.multiprocessing.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepo extends JpaRepository<User, Integer> {
}
