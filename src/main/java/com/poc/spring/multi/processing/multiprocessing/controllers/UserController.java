package com.poc.spring.multi.processing.multiprocessing.controllers;

import com.poc.spring.multi.processing.multiprocessing.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.concurrent.CompletableFuture;

@RestController
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping(
            value = "/users/cf",
            consumes = MediaType.MULTIPART_FORM_DATA_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity saveUsersCF(
            @RequestParam(name = "files") MultipartFile[] files) throws Exception {
        for (MultipartFile file : files) {
            userService.saveUsersCF(file);
        }
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .build();
    }

    @GetMapping(
            value = "/users",
            produces = MediaType.APPLICATION_JSON_VALUE)
    public CompletableFuture<ResponseEntity> findAllUsers() {
        // CompletableFuture<ResponseEntity> =
        return userService.findAllUsers().thenApply(ResponseEntity::ok);
    }
}
